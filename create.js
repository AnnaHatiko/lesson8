var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/lesson8';

MongoClient.connect(url, function (err, database) {
    if (err) {
        console.log('Невозможно подключиться к серверу MongoDB. Ошибка: ', err);
    } else {
        console.log('Соединение установлено для ', url);

        const myDB1 = database.db('myDB3');
        var collection = myDB1.collection('users');

        var user1 = {name: 'Anna', age: 23, gender: 'f'};
        var user2 = {name: 'Lena', age: 33, gender: 'f'};
        var user3 = {name: 'Max', age: 28, gender: 'm'};

        collection.insertMany([user1, user2, user3], function(err, result) {
            if (err) {
                console.log(err);
            } else {
                collection.find().toArray(function(err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("Создан список:\n", result);
                    }
                })
            }

            database.close();
        });
    }
});
