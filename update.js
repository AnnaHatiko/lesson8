var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/lesson8';

MongoClient.connect(url, function (err, database) {
    if (err) {
        console.log('Невозможно подключиться к серверу MongoDB. Ошибка: ', err);
    } else {
        console.log('Соединение установлено для ', url);

        const myDB1 = database.db('myDB3');
        var collection = myDB1.collection('users');

        collection.updateMany({gender: "f"}, {'$set': {name: "Marina"}}, function(err, result) {
            if (err) {
                console.log(err);
            } else {
                collection.find().toArray((function(err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("Обновлен список:\n", result);
                    }
                }))
            }

            database.close();
        });
    }
});